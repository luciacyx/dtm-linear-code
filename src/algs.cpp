#ifndef ALGS_CPP
#define ALGS_CPP

#include "mygraph.cpp"
#include "obj.cpp"
#include <set>
#include <map>
#include <string>
#include <vector>
#include <fstream>
#include <cfloat>

using namespace std;
using namespace mygraph;

enum Algs {SG,QS,BS,SS,MPL,LG,TG,PP,R,QSALT,CK,AL,FKK,QSPP,FRG,FIG,FIGPP, LATG,LR};

resultsHandler allResults;
mutex mtx;

class Args {
public:
   Algs alg;
   string graphFileName;
   string outputFileName = "";
   size_t k = 2;
   tinyGraph g;
   double tElapsed;
   double wallTime;
   Logger logg;
   double epsi = 0.1;
   double delta = 0.1;
   double c = 1;
   size_t N = 1;
   size_t P = 10;
   bool plusplus = false;
   double tradeoff = 0.5;
   bool quiet = false;
   bool lazy = false;
   size_t nThreads = 1;

   Args() {}
   Args( const Args& args ) : alg (args.alg),
			      graphFileName( args.graphFileName ),
			      outputFileName( args.outputFileName ),
			      k( args.k ),
			      g( args.g ),
			      epsi( args.epsi ),
			      delta(args.delta),
			      c(args.c),
			      N(args.N),
			      P(args.P),
			      plusplus(args.plusplus),
			      lazy(args.lazy),
			      nThreads(args.nThreads) {
   }
};

class MyPair {
public:
   node_id u;
   double  gain; //may be negative

   MyPair() {}
   MyPair( node_id a,
	   double g ) {
      u = a;
      gain = g;
   }

   MyPair( const MyPair& rhs ) {
      u = rhs.u;
      gain = rhs.gain;
   }

   void operator=( const MyPair& rhs ) {
      u = rhs.u;
      gain = rhs.gain;
   }
};



struct gainLT {
   bool operator() (const MyPair& p1, const MyPair& p2) {
      return p1.gain < p2.gain;
   }
} gainLTobj;

struct revgainLT {
   bool operator() (const MyPair& p1, const MyPair& p2) {
      return (p1.gain > p2.gain);
   }
} revgainLTobj;


void reportResults( size_t nEvals, size_t obj, size_t maxMem = 0 ) {
   mtx.lock();
   allResults.add( "obj", obj );
   allResults.add( "nEvals", nEvals );
   allResults.add( "mem", maxMem );
   mtx.unlock();
}

void random_set( tinyGraph& g, vector< bool >& C, vector< size_t >& A ) {
  C.assign( g.n, false );
  double prob = 0.5;
  uniform_real_distribution< double > unidist(0, 1);
  for (size_t i = 0; i < A.size(); ++i) {
    if (unidist(gen) < prob) {
      C[ A[i] ] = true;
    }
  }
}


class Rg {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   bool reportRounds = false;
   bool lazy;
   double valSol;
public:
   Rg( Args& args ) : g( args.g ) {
      k = args.k;
      lazy = args.lazy;
   }

   double leastBenefit( node_id u, vector<bool>& set ) {
      ++nEvals;
      set[u] = false;
      double m = marge( nEvals, g, u, set );
      set[u] = true;
      return m;
   }

   void run(bool report = true) {
      if (!lazy)
	 run_standard();
      else
	 run_lazy();

      if (report)
	 reportResults( nEvals, valSol );
      
   }
   
   void run_standard() {
      vector<bool> A( g.n, false );
      vector< MyPair > margeGains;
      MyPair tmp;

      vector< double > valRounds;
      for (size_t i = 0; i < k; ++i) {
	 if (reportRounds) {
	    //report_rounds( A, valRounds, g );
	 }
	 
	 margeGains.clear();
	 for (node_id u = 0; u < g.n; ++u) {
	    if (!( A[u] )) {
	       tmp.gain = marge( nEvals, g,u, A);
	       tmp.u = u;
	       margeGains.push_back( tmp );
	    }
	 }

	 std::sort( margeGains.begin(), margeGains.end(), revgainLT() );
	 uniform_int_distribution< size_t > dist(0, k - 1);
	 size_t rand = dist( gen );
	 node_id u = margeGains[ rand ].u;
	 A[u] = true;
      }

      valSol = compute_valSet( --nEvals,  g, A );



      if (reportRounds) {
	 // for (size_t j = 0; j < valRounds.size(); ++j) {
	 //    allResults.add( to_string( j ), valRounds[ j ] );
	 // }
      }
   }

   void run_lazy() {
      vector<bool> A( g.n, false );

      //initialize the priority queue
      priority_queue< MyPair, vector< MyPair >, gainLT > Q;
      for (node_id u = 0; u < g.n; ++u) {
	 MyPair tmp;
	 tmp.u = u;
	 tmp.gain = DBL_MAX;
	 Q.push( tmp );
      }

      vector< MyPair > topGains;
      MyPair tmp;

      for (size_t i = 0; i < k; ++i) {
	 vector< bool > current(g.n, false );
	 bool bbreak = false;
	 topGains.clear();
	 while (!bbreak) {
	    tmp = Q.top();
	    Q.pop();
	    if (!( A[tmp.u] )) {
	       if (current[ tmp.u ] ) {
		  topGains.push_back( tmp );
		  if (topGains.size() == k) {
		     bbreak = true;
		  }
	       } else {
		  tmp.gain = marge( nEvals, g, tmp.u, A);
		  Q.push( tmp );
		  current[tmp.u] = true;
	       }
	    } 
	 }
	 
	 uniform_int_distribution< size_t > dist(0, k - 1);
	 size_t rand = dist( gen );
	 node_id u = topGains[ rand ].u;
	 A[u] = true;
	 for (size_t j = 0; j < topGains.size(); ++j) {
	    Q.push( topGains[j] );
	 }
      }

      valSol = compute_valSet( --nEvals,  g, A );

   }
};

class Frg {
   random_device rd;
   mt19937 gen;
   Args& myArgs;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   size_t w;
   size_t W;
   bool lazy;
public:
   Frg( Args& args ) : gen(rd()), myArgs( args ), g( args.g ) {
      k = args.k;
      epsi = args.epsi;
      lazy = args.lazy;
   }

   long leastBenefit( node_id u, vector<bool>& set ) {
      ++nEvals;
      set[u] = false;
      long m = marge( nEvals, g, u, set );
      set[u] = true;
      return m;
   }

   void fillM( vector< node_id >& M, vector< bool >& S ) {

      while (w > epsi*W / k) {
	 for (node_id x = 0; x < g.n; ++x) {
	    if (marge( nEvals, g, x, S ) > (1 - epsi)*w) {
	       M.push_back( x );
	       if ( M.size() >= k )
		  return;
	    }
	 }
	 
	 w = (1 - epsi)*w;
      }
   }

   void run() {
      runRandom();
   }

   void randomSampling(double p, size_t s, vector<bool>& A ) {
      size_t rho = p * g.n + 1;
      vector< bool > M;
      vector< MyPair > margeGains;
      MyPair tmp;
      vector< bool > cvgA( g.n, false);
      for (size_t i = 0; i < k; ++i) {
	 sampleUnifSize( M, rho );

	 margeGains.clear();
	 for (node_id u = 0; u < g.n; ++u) {
	    if ( M[u] ) {
	       tmp.gain = marge( nEvals, g,u, A, cvgA);
	       tmp.u = u;
	       margeGains.push_back( tmp );
	    }
	 }

	 std::sort( margeGains.begin(), margeGains.end(), revgainLT() );
	 uniform_int_distribution< size_t > dist(0, s - 1);
	 size_t rand = dist( gen );
	 node_id u = margeGains[ rand ].u;
	 A[u] = true;
      
      }
   }

   void randomSampling_lazy(double p, size_t s, vector<bool>& A ) {
      //initialize the priority queue
      priority_queue< MyPair, vector< MyPair >, gainLT > Q;
      for (node_id u = 0; u < g.n; ++u) {
	 MyPair tmp;
	 tmp.u = u;
	 tmp.gain = DBL_MAX;
	 Q.push( tmp );
      }

      size_t rho = p * g.n + 1;
      vector< bool > M;

      MyPair tmp;

      for (size_t i = 0; i < k; ++i) {
	 sampleUnifSize( M, rho );

	 uniform_int_distribution< size_t > dist(0, s - 1);
	 size_t rand = dist( gen );
	 vector< bool > current(g.n, false );
	 bool bbreak = false;
	 vector< MyPair > extracted_elements;
	 vector< MyPair > top_elements;
	 while (!bbreak) {
	    
	    MyPair tmp = Q.top();
	    //	    cerr << tmp.u << ' ' << current[ tmp.u] << ' '
	    //		 << M[tmp.u] << ' ';
	    Q.pop();
	    //cerr << endl << Q.size() << endl;
	    if ( M[tmp.u] ) {
	       if (current[ tmp.u ]) {
		  top_elements.push_back( tmp );
		  if (top_elements.size() == (rand + 1)) {
		     A[ tmp.u ] = true;
		     bbreak = true;
		  }
	       }
	       else {
	
		  tmp.gain = marge( nEvals, g, tmp.u, A );
		  Q.push( tmp );
		  current[ tmp.u ] = true;
	       }
	    } else {
	       
	       //ignore this element for now
	       extracted_elements.push_back( tmp );
	    }
	 }

	 //add extracted elements back to queue
	 for (size_t j = 0; j < extracted_elements.size(); ++j) {
	    Q.push( extracted_elements[j] );
	 }
	 //add top elements back to queue
	 for (size_t j = 0; j < top_elements.size(); ++j) {
	    Q.push( top_elements[j] );
	 }
      }
   }

   void sampleUnifSize( vector<bool>& R, size_t Size ) {

      if (Size >= g.n - 1) {
	 R.assign(g.n, true);
	 return;
      }
      
      uniform_int_distribution<size_t> dist(0, g.n-1);
      R.assign(g.n, false);

      for (size_t i = 0; i < Size; ++i) {
	 size_t pos;
	 do {
	    pos = dist( gen );
	 } while ( R[ pos ] );
	 R[ pos ] = true;
      }
    
   }

   void runRandom() {
      double p = 8.0 / (k * epsi * epsi) * log( 2 / (epsi) );
      g.logg << "FastRandom: p = " << p << endL;
      if (p >= 1.0) {
	 //run RandomGreedy
	 g.logg << "FastRandom: Running RandomGreedy..." << endL;
	 Rg rg( myArgs );
	 rg.run();
	 
      } else {
	 g.logg << "FastRandom: Running RandomSampling..." << endL;
	 vector<bool> S(g.n, false );
	 size_t rho = p * g.n + 1;
	 size_t s = rho * k / g.n;

	 if (!lazy)
	    randomSampling( p, s, S );
	 else
	    randomSampling_lazy( p, s, S );

	 g.logg << "S: " << compute_valSet( nEvals,  g, S ) << endL;
	 g.logg << "Evals: " << nEvals << endL;

	 reportResults( nEvals, compute_valSet(nEvals, g, S), k );
      }

   }   
   
};

class LRVZ_mon {
   random_device rd;
   mt19937 gen;
   Args& myArgs;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   size_t w;
   size_t W;
   bool lazy;
   double alpha;
   bool sample = true;
   vector< double > f;
   
public:
   LRVZ_mon( Args& args ) : gen(rd()), myArgs( args ), g( args.g ) {
      k = args.k;
      epsi = args.epsi

;
      lazy = args.lazy;
      alpha = 1 / epsi;
      f.push_back( 0 );
   }

   void setz( size_t idx, size_t& zlow, size_t& zhigh ) {
      int z_low = floor( idx / alpha ) - 20*alpha*sqrt(k*log(k) / log(2.0) );
      if (z_low < 0)
	 z_low = 0;

      size_t z_high = ceil( idx / alpha) + 20*alpha*sqrt(k*log(k)/log(2.0));
      if (z_high > k)
	 z_high = k - 1;
      zlow = z_low;
      zhigh = z_high;
   }
   
   void process_window( size_t idx, //window index
			vector< node_id >& w, //window
			vector< vector< vector< bool > > >& L,
			vector< vector< vector< node_id > > >& Lids,
			vector< node_id >& H ) { //history
         
      uniform_real_distribution< double > unidist(0, 1);
      vector< node_id > C;
      if (!sample)
	 C = w;
      else {
	 for (size_t l = 0; l < w.size(); ++l) {
	    node_id e = w[l];
	    vector< double > qe;
	    qe.push_back( 1.0 ); //qe1 = 1
	    vector< double > xe;
	    for (size_t j = 1; j <= idx; ++j) {
	       xe.push_back( unidist( gen ) );
	    }

	    for (size_t j = 1; j<= idx; ++j) {
	       vector< size_t > Aej;
	       for (size_t r = 1; r < j; ++r) {
		  if ( r >= j)
		     break;
		  if ( xe[r - 1] > qe[r - 1] ) {
		     Aej.push_back( r );
		  } else {
		     size_t z_lr, z_hr;
		     setz( r, z_lr, z_hr );
		     double sum = 0;
		     for (size_t ell = z_lr; ell <= z_hr; ++ell) {
			sum += marge( nEvals, g, e, L[ r - 1 ][ ell ] );
		     }
		     if (sum < f[r])
			Aej.push_back( r );
		  }
	       }
	       qe.push_back( (alpha*k - j + Aej.size() + 1) / (alpha*k) );
	    }

	    if ( xe[ idx - 1 ] <= qe[ idx - 1 ] ) {
	       C.push_back( e );
	    }
	 }
      }
      
      for (size_t ell = 0; ell <= k; ++ell) {
	 L[ idx ][ ell ] = L[ idx - 1][ ell ];
	 Lids[ idx ][ ell ] = Lids[ idx - 1][ ell ];
      }

      vector< node_id > R;
      double prob = 1.0 / (alpha*k);
      for (size_t j = 0; j < H.size(); ++j) {
	 if (unidist(gen) < prob) {
	    R.push_back( H[j] );
	    C.push_back( H[j] );
	 }
      }

      size_t z_low;
      size_t z_high;
      setz( idx, z_low, z_high );

      node_id estar = -1;
      double maxgain = -DBL_MAX;
      for (size_t i = 0; i < C.size(); ++i) {
	 node_id e = C[i];
	 double gain_e = 0;
	 for (size_t ell = z_low; ell <= z_high; ++ell) {
	    gain_e += marge( nEvals, g, e, L[ idx - 1 ][ell] );
	 }
	 if (gain_e > maxgain) {
	    estar = e;
	    maxgain = gain_e;
	 }
      }
      if (estar == -1)
	 return;
      f.push_back( maxgain );
      
      double sum_1 = 0;
      for (size_t ell = z_low; ell <= z_high; ++ell) {
	 vector< node_id > testSet = Lids[ idx - 1 ][ ell ];
	 //testSet[ estar ] = true;
	 testSet.push_back( estar );
	 sum_1 += compute_valSet( nEvals, g, testSet );
      }

      double sum_2 = 0;
      for (size_t ell = z_low; ell <= z_high; ++ell) {
	 vector< node_id > testSet = Lids[ idx - 1 ][ ell + 1 ];
	 sum_2 += compute_valSet( nEvals, g, testSet );
      }

      if (sum_1 > sum_2) {
	 //cerr << sum_1 << ' ' << sum_2 << ' ' << idx << ' ' << estar << endl;
	 H.push_back( estar );
	 for (size_t ell = z_low; ell <= z_high; ++ell) {
	    L[idx][ell + 1] = L[idx - 1][ell];
	    L[idx][ell + 1][estar] =true;
	    Lids[idx][ell + 1] = Lids[idx - 1][ell];
	    Lids[idx][ell + 1].push_back( estar );
	 }
	 //	 for (size_t ell = 0; ell < k; ++ell) {
	 for (size_t ell = z_low; ell <= z_high; ++ell) {
	    if (compute_valSet( nEvals, g, Lids[idx][ell] ) > compute_valSet( nEvals, g, Lids[ idx ][ ell + 1 ] ) ) {
	       estar = -1;
	       maxgain = -DBL_MAX;
	       for (size_t j = 0; j < Lids[idx][ell + 1].size(); ++j) {
		  double gain = marge( nEvals, g, Lids[idx][ell+1][j], L[ idx ][ell] );
		     if (gain > maxgain) {
			estar = j;
			maxgain = gain;
		     }
		  
	       }
	       L[idx][ell + 1] = L[ idx ][ ell ];
	       L[idx][ell + 1][estar] = true;
	       Lids[idx][ell + 1] = Lids[ idx ][ ell ];
	       Lids[idx][ell + 1].push_back(estar);
	    }
	 }
      }
   }

   void run() {
      //create stream
      vector< node_id > v_stream(g.n, 0);
      for (size_t i = 0; i < g.n; ++i) {
	 v_stream[i] = i;
      }

      shuffle( v_stream.begin(), v_stream.end(), gen );

      g.logg << "Random stream created." << endL;
      
      //create windows
      g.logg << "Creating windows..." << endL;
      vector< vector< node_id > > v_windows;
      size_t m = ceil( ((double)alpha*k) );
      size_t window_size = floor( g.n / ((double) m) );
      if (m > g.n) {
	 m = g.n;
	 window_size = 1;
      }
	 
      g.logg << "m= " << m << endL;

      g.logg << "Window size= " << window_size << endL;

      for (size_t i = 0; i < m; ++i) {
	 vector<node_id>::iterator v_start = v_stream.begin() + i*window_size;
	 vector<node_id>::iterator v_end;
	 if ( i == m-1 ) {
	    v_end = v_stream.end();
	 } else {
	    v_end = v_stream.begin() + (i+1)*window_size;
	 }

	 v_windows.push_back( vector<node_id>( v_start, v_end ) );
      }

      //create L
      g.logg << "Creating L..." << endL;
      vector< bool > emptySet(g.n, false);
      vector< node_id > emptyId;
      vector< vector< bool > > ellSet( k + 1, emptySet );
      vector< vector< node_id > > ellSetId( k + 1, emptyId );
      vector< vector< vector< bool > > > L( m + 1, ellSet );
      vector< vector< vector< node_id > > > Lid( m + 1, ellSetId );
      

      vector< node_id > H;
      for (size_t idx = 1; idx <= m; ++idx) {
	 if ( idx % (m / 50) == 0 ) {
	    cerr << "\r                                         \r"
		 << setprecision( 3 )
		 << (float) idx / m * 100 << " %";
	 }
	 process_window( idx, v_windows[ idx - 1 ], L, Lid, H );
      }

      size_t ellstar = -1;
      double maxval = -DBL_MAX;
      for (size_t ell = 1; ell <= k; ++ell) {
	 double val = compute_valSet(nEvals, g, Lid[m][ell]);
	 if (val > maxval) {
	    ellstar = ell;
	    maxval = val;
	 }
      }

      g.logg << "Solution value: " << maxval << endL;
      g.logg << "Queries: " << nEvals << endL;

      reportResults( nEvals, maxval );
   }
   
};


class Rand {
   random_device rd;
   mt19937 gen;
   Args& myArgs;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;

public:
   Rand( Args& args ) : gen(rd()), myArgs( args ), g( args.g ) {
      k = args.k;
   }

   void sampleUnifSize( vector<bool>& R, size_t Size ) {

      if (Size >= g.n - 1) {
	 R.assign(g.n, true);
	 return;
      }
      
      uniform_int_distribution<size_t> dist(0, g.n-1);
      R.assign(g.n, false);

      for (size_t i = 0; i < Size; ++i) {
	 size_t pos;
	 do {
	    pos = dist( gen );
	 } while ( R[ pos ] );
	 R[ pos ] = true;
      }
    
   }
   void sampleUnifSize( vector<node_id>& R, size_t Size,
			uniform_int_distribution<size_t>& dist ) {
      for (size_t i = 0; i < Size; ++i) {
	 size_t pos = dist( gen );
	 swap( R[i], R[pos] );
      }
    
   }
   
   void run() {
      double solVal = 0;
      size_t max = 1;
      vector< node_id > uni;
      uni.reserve(g.n);
      for (node_id u = 0; u < g.n; ++u) {
	 uni.push_back( u );
      }

      vector< node_id > tmp;
      uniform_int_distribution< size_t > dist(0, g.n - 1);
      for (size_t i = 0; i < max; ++i) {

	 sampleUnifSize( uni, k, dist );
	 tmp.assign( uni.begin(), uni.begin() + k );
	 double tmpVal = compute_valSet( nEvals, g, tmp );
	 if (tmpVal > solVal)
	    solVal = tmpVal;
      }
      g.logg << "Random solution value: " << solVal << endL;
      reportResults( nEvals, solVal );
   }
   
};


//Standard Greedy
class Sg {
public:
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double solVal;
   bool lazy;
   
   Sg( Args& args ) : g( args.g ) {
      k = args.k;
      lazy = args.lazy;
   }

   void runlazy( vector< bool >& restricted_uni ) {
            vector<bool> A( g.n, false );
      vector<bool> cvgA( g.n, false );
      
      double maxGain;
      node_id maxIdx;
      MyPair tmp;

      priority_queue< MyPair, vector< MyPair >, gainLT > Q;
      //initialize the priority queue
      for (size_t u = 0; u < g.n; ++u) {
	 if (restricted_uni[u]) {
	    MyPair tmp;
	    tmp.u = u;
	    tmp.gain = DBL_MAX;
	    Q.push( tmp );
	 }
      }
      
      for (size_t i = 0; i < k; ++i) {
	 vector< bool > current(g.n, false );
	 bool bbreak = false;
	 while (!bbreak) {
	    MyPair tmp = Q.top();
	    Q.pop();
	    if (current[ tmp.u ]) {
	      if (tmp.gain < 0) {
		bbreak = true;
	      } else {
		A[ tmp.u ] = true;
		bbreak = true;
	      }
	    } else {
	       tmp.gain = marge( nEvals, g, tmp.u ,A );
	       Q.push( tmp );
	       current[ tmp.u ] = true;
	    }
	 }
      }

      solVal = compute_valSet( --nEvals,  g, A );
   }

   void run_standard( vector< bool >& restricted_uni ) {
      vector<bool> A( g.n, false );

      double maxGain;
      node_id maxIdx;
      MyPair tmp;

      vector< double > valRounds;

      for (size_t i = 0; i < k; ++i) {
	 maxGain = 0;
	 for (node_id u = 0; u < g.n; ++u) {
	    if (restricted_uni[u]) {
	       if (marge( nEvals, g,u,A) > maxGain) {
		  maxIdx = u;
		  maxGain = marge( nEvals, g,u,A);
	       }
	    }
	 }

	 if (maxGain > 0) {
	    A[maxIdx] = true;
	 } else {
	    break;
	 }
      }

      solVal = compute_valSet( --nEvals,  g, A );
   }
   
   void run(bool b_reportResults = true, vector< bool >& restricted_uni = emptySetVector) {
      if (restricted_uni.size() == 0) {
	 restricted_uni.assign(g.n, true );
      }
      if (lazy)
	 runlazy( restricted_uni );
      else
	 run_standard( restricted_uni );

      g.logg << "Evals: " << nEvals << endL;
      
      g.logg << "solVal: " << solVal << endL;

      if (b_reportResults)
	 reportResults( nEvals, solVal );

      restricted_uni.clear();
   }
};

class qs_set {
public:
   vector< node_id > ids;
   vector< bool > set;
   size_t size = 0;
   double val = 0.0;
   size_t maxSize = 0;
   size_t n;
   
   qs_set( size_t in_maxS, size_t n_in ) {
      n = n_in;
      maxSize = in_maxS;
      set.assign(n, false );
   }

   void reset() {
      set.assign(n, false );
      val = 0;
      size = 0;
      ids.clear();
   }
   
   double marginal_gain( size_t& nEvals, tinyGraph& g, vector< size_t >& ids_in, bool optimize = false ) {
      if (ids_in.size() > 1) {
	 double gain = 0;
	 if (!optimize) {
	    vector< node_id > tmpids = ids;
	    for (size_t i = 0; i < ids_in.size(); ++i) {
	       tmpids.push_back( ids_in[i ] );
	    }
	    double valTmp = compute_valSet( nEvals, g, tmpids );
	    gain = valTmp - val;
	 } else {
	    vector< node_id > added_ids;
	    size_t tmpEvals = 0;
	    for (size_t i = 0; i < ids_in.size(); ++i) {
	       if (!set[ ids_in[i] ]) {
		  gain += marge( tmpEvals, g, ids_in[i], set );
		  added_ids.push_back( ids_in[i] );
		  set[ ids_in[i] ] = true;
	       }
	    }

	    for (size_t i = 0; i < added_ids.size(); ++i) {
	       set[ added_ids[i] ] = false;
	    }
	    ++nEvals;
	 }
	 return gain;
      } else {
	 return marge( nEvals, g, ids_in[0], set );
      }
   }

   double marginal_gain( size_t& nEvals, tinyGraph& g,  size_t id_in ) {
      return marge( nEvals, g, id_in, set );
   }

   bool add( vector< size_t >& ids_in, double newVal ) {
      bool bDel = false;
      for (size_t i = 0; i < ids_in.size(); ++i) {
	 ids.push_back( ids_in[i ] );
	 set[ ids_in[i] ] = true;
      }
      val = newVal;

      if (maxSize > 0) {
	 if (ids.size() > maxSize) {
	    bDel = true;
	    ids.assign( ids.begin() + maxSize / 2, ids.end() );
	    set.assign( n, false );
	    for (size_t i = 0; i < ids.size(); ++i) {
	       set[ ids[i] ] = true;
	    }
	 }
	 size = ids.size();
      }

      return bDel;
   }

   bool add( size_t id_in, double newVal, size_t& nEvals, tinyGraph& g ) {
      bool bDel = false;
      ids.push_back( id_in );
      set[ id_in ] = true;
      
      val = newVal;

      if (maxSize > 0) {
	 if (ids.size() > maxSize) {
	    bDel = true;
	    ids.assign( ids.begin() + maxSize / 2, ids.end() );
	    set.assign( n, false );
	    for (size_t i = 0; i < ids.size(); ++i) {
	       set[ ids[i] ] = true;
	    }
	 }
	 size = ids.size();
      }

      if (bDel) {
	 val = compute_valSet( nEvals, g, set );
      }

      return bDel;
   }
};

class linear_unc {
   Args& args;
   qs_set A;
   qs_set B;
   size_t nEvals;
   tinyGraph& g;
   double maxVal = 0;
public:   
   linear_unc (Args& inargs) : args(inargs), A(0, args.g.n), B(0,args.g.n), nEvals(0), g(args.g) {
      
   }

   double update( size_t& nEvals, size_t e ) {
      double gainA = A.marginal_gain( nEvals, g, e );
      double gainB = B.marginal_gain( nEvals, g, e );
      if (gainA >= gainB) {
	 if (gainA >= 0) {
	    A.add( e, A.val + gainA, nEvals, g );
	    if (A.val > maxVal) {
	       maxVal = A.val;
	    }
	 }  
      } else {
	 if (gainB >= 0) {
	    B.add( e, B.val + gainB, nEvals, g );
	    if (B.val > maxVal) {
	       maxVal = B.val;
	    }
	 }  
      }

      return maxVal;
   }

   double run( size_t& nEvals, vector<node_id>& uni ) {
      A.reset();
      B.reset();
      maxVal = 0;
      for (size_t i = 0; i < uni.size(); ++i) {
	 size_t e = uni[i];
	 update( nEvals, e );
      }

      return maxVal;
   }
};

/*
 * FastTripleGreedy
 */
class FTg {
   public:
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double solVal;
   double epsi;
   double gamma;
   double alpha;
   bool lazy;
   
   FTg( Args& args, double Gamma_in = 0, double alpha_in = 0 ) : g( args.g ) {
      k = args.k;
      epsi = args.epsi;
      gamma = Gamma_in;
      alpha = alpha_in;
      lazy = args.lazy;
   }

   double leastBenefit( node_id u, vector<bool>& set ) {
      set[u] = false;
      double m = marge( nEvals, g, u, set );
      set[u] = true;
      return m;
   }

   void uncMax( vector<bool>& set ) {
      vector<bool> all(set);
      vector<bool> none( g.n, false );
      
      for (size_t u = 0; u < g.n; ++u) {
	 if (set[u]) {
	    double margeA = marge( nEvals, g, u, none ); //adding u to A
	    vector<bool> allMinus ( all );
	    allMinus[u] = false;
	    double margeB = static_cast<double>(compute_valSet(nEvals, g, allMinus)) - compute_valSet(nEvals,g, all );
	    if (margeA >= margeB) {
	       none[u] = true;
	    } else {
	       all[u] = false;
	    }
	 }
      }

      set = all;
   }
   
   void run( vector< bool >& restricted_uni = emptySetVector) {
      if (restricted_uni.size() == 0) {
	 restricted_uni.assign(g.n, true );
      }

      vector<bool> A( g.n, false );
      size_t sizeA = 0;

      vector< double > pastGains(g.n, DBL_MAX);
      
      if (gamma == 0) {
	 //Get max singleton
	 g.logg << "FastIteratedGreedy: Determining max singleton..." << endL;
	 double M = 0;

	 for (size_t x = 0; x < g.n; ++x) {
	    if (restricted_uni[x]) {
	       double sval = marge( nEvals, g, x, A );
	       if ( sval  > M ) {
		  M = sval;
	       }
	       pastGains[x] = sval;
	    }
	 }
	 gamma = M;
	 alpha = 1.0/k;
      } 

      g.logg << "gamma= " << gamma << endL;
      double tau = gamma / alpha / k;
      vector< size_t > idsA;

      double gain;
      while (tau >= epsi*gamma / k) {
	 tau = tau * ( 1 - epsi );
	 for (node_id u = 0; u < g.n; ++u) {
	    gain = 0;
	    if (restricted_uni[u]) {
	       if (pastGains[u] >= tau || !lazy) {
		  gain = marge(nEvals, g, u, A);
		  if (gain >= tau) {
		     A[u] = true;
		     ++sizeA;
		     idsA.push_back( u );
		  }
		  if (sizeA == k)
		     break;
		  pastGains[u] = gain;
	       }
	    }
	 }
	 if (sizeA == k)
	    break;
      }


      
      vector<bool> B( g.n, false );
      size_t sizeB = 0;
      tau = gamma / alpha / k;

      pastGains.assign(g.n, tau + 1);
      while (tau >= epsi*gamma / k) {
	 tau = tau * ( 1 - epsi );
	 for (node_id u = 0; u < g.n; ++u) {
	    gain = 0;
	    if (!A[u] && restricted_uni[u]) {
	       if (pastGains[u] >= tau || !lazy) {
		  gain = marge(nEvals, g, u, B);
		  if (gain >= tau) {
		     B[u] = true;
		     ++sizeB;
		  }
		  pastGains[u] = gain;
	       }
	    }
	    if (sizeB == k)
	       break;
	    
	 }
	 if (sizeB == k)
	    break;

      }

      vector<bool> Amax;
      random_set( g, Amax, idsA);

      vector< double > vals;
      vals.push_back( compute_valSet( nEvals,  g, A ) );
      vals.push_back( compute_valSet( nEvals,  g, Amax ) );
      vals.push_back( compute_valSet( nEvals,  g, B ) );
      size_t maxVal = 0;
      g.logg << "Vals: ";
      for (size_t i = 0; i < vals.size(); ++i) {
	 g.logg << vals[i] << " ";
	 if (vals[i] > maxVal) {
	    maxVal = vals[i];
	 }
      }
      
      g.logg << endL;
      g.logg << "Evals: " << nEvals << endL;

      //reportResults( nEvals, maxVal );
      solVal = maxVal;
   }
};




//LinearStream
class Qs {
public:
   Args& args;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   size_t c = 1;
   bool plusplus;
   double solVal = 0.0;
   double epsi;
   double delta;
   size_t ell;
   size_t maxMem = 0;
   
   Qs( Args& inargs ) : args(inargs), g( args.g ) {
      k = inargs.k;
      c = inargs.c;
      plusplus = inargs.plusplus;
      epsi = inargs.epsi;
      delta = inargs.delta;
      double alpha = 1.0 + 4.0 / delta; //delta = b of paper
      ell = log( 6*alpha / epsi + 1 ) / log( 2 ) + 4;
   }

   double run_BR( vector< bool >& U, // restricted universe
		  double Gamma, //preliminary value
		  double alpha ) { //preliminary ratio
      double tau = Gamma / (alpha * k);

      vector< bool > A( g.n, false );
      vector< bool > cvgA( g.n, false );
      double gamma = tau;
      double valA = 0;
      size_t sizeA = 0;
      size_t maxGain;
      size_t passes = 0;

      vector< double > lastGain( g.n, tau );
      while (tau >= Gamma / (8.0 * k)) {
	 ++passes;
	 tau = tau * ( 1 - epsi );
	 for (node_id u = 0; u < g.n; ++u) {
	    if (U[u]) {
	       if (tau <= lastGain[u]) {
		  double gain = marge(nEvals, g, u, A, cvgA);
		  lastGain[u] = gain;
		  if (gain >= tau) {
		     A[u] = true;
		     g.coverAdjacent( u, cvgA );
		     valA += gain;
	       
		     ++sizeA;
		  }
		  if (sizeA == k)
		     break;
	       }
	    }
	 }
	 if (sizeA == k)
	    break;
      }
      

      g.logg << "Passes: " << passes << endL;
      size_t tmp = 0;
      double solVal = compute_valSet( tmp, g, A );

      return solVal;
   }


   void run( bool ); 

};

class Br {
public:
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   Qs qs;
   bool lazy;
   size_t maxMem = 0;
   double solVal = 0;

   Br( Args& args ) : g( args.g ), qs( args ) {
      k = args.k;
      epsi = args.epsi;
      lazy = args.lazy;
   }

   void run_mpl( vector< bool >& rest_uni, double Gamma, double alpha ) {
      double tau, taumin;
      if (monotone) {
	 tau = Gamma / (alpha * k);
	 taumin = Gamma / (8.0*k);
      } else {
	 tau = Gamma / (4 * alpha * k);
	 taumin = epsi * Gamma / (16.0*k);
      }

      vector< bool > A( g.n, false );
      vector< bool > B( g.n, false );
      size_t sizeB = 0;
      vector< bool > cvgA( g.n, false );
      double gamma = tau;
      unsigned passes = 0;

      double valA = 0;
      double valB = 0;
      size_t sizeA = 0;
      size_t maxGain;

      vector< double > pastGainsA( g.n, tau + 1);
      vector< double > pastGainsB( g.n, tau + 1);
      
      while (tau > taumin) {
	 ++passes;
	 maxGain = 0;
	 tau = tau * ( 1 - epsi );
	 for (node_id u = 0; u < g.n; ++u) {
	    if (rest_uni[u]) {
	       double gainA = 0;
	       if (sizeA < k) {
		  if (pastGainsA[u] >= tau || !lazy) {
		     gainA = marge(nEvals, g, u, A);
		     pastGainsA[u] = gainA;
		  }
	       }
	       double gainB = 0;
	       if (!monotone) {
		  if (sizeB < k) {
		     if (pastGainsB[u] >= tau || !lazy) {
			gainB = marge(nEvals, g, u, B );
			pastGainsB[u] = gainB;
		     }
		  }
	       }
	       if (gainA >= gainB) {
		  if (gainA >= tau) {
		     A[u] = true;
		     valA += gainA;
		     ++sizeA;
		  }
	       } else {
		  if (gainB >= tau) {
		     B[u] = true;
		     valB += gainB;
		     ++sizeB;
		  }
	       }
	    }
	 }
      }

      g.logg << "Passes: " << passes << endL;
      g.logg << "Evals: " << nEvals << endL;
      size_t tmp = 0;
      g.logg << "f(A): " << valA << endL;
      g.logg << "f(B): " << valB << endL;


      if (valA > valB) 
	 solVal = compute_valSet( tmp, g, A );
      else 
	 solVal = compute_valSet( tmp, g, B );
      
      g.logg << "Solution Value: " << solVal << endL;
      
   }
   
   void run() {
      size_t passes = 0;

      qs.epsi = 0.01;
      qs.plusplus = false;
      //      if (!monotone)
      qs.c = 64;
      qs.run( false );
      double Gamma = qs.solVal;
      nEvals = qs.nEvals;
      ++passes;
      //compute ratio of 
      double alpha;
      if (monotone) {
	 if (k > 8.0 * qs.c / exp(1)) { //ratio of QuickStreamLargeK
	    alpha = 1.0 / (1.0 + qs.c + 1.0 / (k*k*k - 1) );
	    alpha = alpha * (1.0 - 1.0 / exp(1) - (2.0 * qs.c) / (k*exp(1)) - qs.c*qs.c / (k*k*exp(1)));
	 } else { 
	    alpha = 0.25 - qs.epsi;
	    alpha = alpha / qs.c;
	 }
      } else {
	 alpha = 0.1 / qs.c;
      }

      vector< bool > uni(g.n, true);
      run_mpl( uni, Gamma, alpha );

      reportResults( nEvals, solVal );
   }

};

void Qs::run( bool report = true ) {
      size_t maxSize = 2.0*c*ell*(k / delta + 1)*log( static_cast<double>(k) ) / log(2) + 1;
      
      qs_set A(maxSize, g.n);
      qs_set B(maxSize, g.n);
      double um_A = 0;
      double um_B = 0;
      double tau = 0;
      linear_unc lin_A(args);
      linear_unc lin_B(args);
      for (node_id u = 0; u < g.n; ++u) {
	 double gainA = A.marginal_gain( nEvals, g, u );
	 double gainB = B.marginal_gain( nEvals, g, u );
	 
	 if (gainA >= gainB) {
	    if (gainA >= delta * tau / k) {
	       bool bDel = A.add( u, A.val + gainA, nEvals, g );
	       if (A.val > tau)
		  tau = A.val;
	       if (bDel) {
		  g.logg << "Deletion occurred." << endL;
		  um_A = lin_A.run( nEvals, A.ids );
	       } else {
		  um_A = lin_A.update( nEvals, u );
	       }
	       if (um_A > tau)
		  tau = um_A;
	    }
	 } else {
	    if (gainB >= delta * tau / k) {
	       bool bDel = B.add( u, B.val + gainB, nEvals, g );
	       if (B.val > tau)
		  tau = B.val;
	       if (bDel) {
		  g.logg << "Deletion occurred." << endL;
		  um_B = lin_B.run( nEvals, B.ids );
	       } else {
		  um_B = lin_B.update( nEvals, u );
	       }
	       if (um_B > tau)
		  tau = um_B;
	    }
	 }

	 if (A.ids.size() + B.ids.size() > maxMem) {
	    maxMem = A.ids.size() + B.ids.size();
	 }
      }

      vector< node_id > Aprime;
      vector< node_id > Bprime;

      if (A.size > k) {
	 Aprime.assign( A.ids.end() - (k), A.ids.end() );

      }
      else {

	 Aprime.assign( A.ids.begin(), A.ids.end() );
      }

      if (B.size > k) {
	 Bprime.assign( B.ids.end() - (k), B.ids.end() );

      }
      else {
	 Bprime.assign( B.ids.begin(), B.ids.end() );

      }

      g.logg << "Size of A: " << A.size << endL;
      g.logg << "Size of Aprime: " << Aprime.size() << endL;
      g.logg << "Size of B: " << B.size << endL;
      g.logg << "Size of Bprime: " << Bprime.size() << endL;

      double valAp =compute_valSet( nEvals, g, Aprime );
      double valBp = compute_valSet( nEvals, g, Bprime );


      double valSol;
      size_t sizeSol;
      if (valAp > valBp) {
	 valSol = valAp;
	 sizeSol = Aprime.size();
      } else {
	 valSol = valBp;
	 sizeSol = Bprime.size();
      }

      g.logg << "Solution value before post: " << valSol << endL;

      if (plusplus) {
	 vector< bool > rest_uni(g.n, false);
	 for (size_t i = 0; i < A.ids.size(); ++i) {
	    rest_uni[A.ids[i]] = true;
	 }
	 for (size_t i = 0; i < B.ids.size(); ++i) {
	    rest_uni[B.ids[i]] = true;
	 }

	 Br br( args );
	 br.run_mpl( rest_uni, valSol, 0.1 );
	 valSol = br.solVal;
	 nEvals += br.nEvals;
	    
	    // FTg ftg( args, valSol, 0.1 );
	    // ftg.run( rest_uni );
	    // valSol = ftg.solVal;
	    // nEvals += ftg.nEvals;
	    
	    // Sg sg( args );
	    // sg.run( false, rest_uni );
	    // valSol = sg.solVal;
	    // nEvals += sg.nEvals;
      }
      
      g.logg << "nEvals: " << nEvals << endL;
      g.logg << "valSol: " << valSol << endL;
      g.logg << "sizeSol: " << sizeSol << endL;

      if (report)
	 reportResults( nEvals, valSol, maxMem );

      solVal = valSol;
   }

class Fkk {
   public:
   random_device rd;
   mt19937 gen;
   Args& args;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double q;
   double c;
  uniform_real_distribution< double > unidist;
  vector< bool > partialA;
  
  Fkk( Args& inargs ) : gen(rd()), args(inargs), g( args.g ), unidist(0,1) {
      k = inargs.k;
      if (monotone) {
	 c = 1.0;
	 q = 1.0/(3);
      } else {
	 c = sqrt( 2 );
	 q = 1.0/(1.0 + sqrt(2) + 1); //cardinality
      }
      partialA.assign(g.n, false );
   }

   void exchange_candidate( node_id& x, double& gainx,
			    vector<node_id>& idsA ) {
      gainx = DBL_MAX; //infinity
      for (size_t i = 0; i < idsA.size(); ++i) {
	double tmpGain = marge(nEvals, g, idsA[i], partialA );
	if (tmpGain < gainx) {
	  gainx = tmpGain;
	  x = idsA[i];
	}
	partialA[idsA[i]] = true;
      }

      for (size_t i = 0; i < idsA.size(); ++i) {
	partialA[ idsA[i] ] = false;
      }
   }

   void run( bool report = true ) {
      vector< bool > A(g.n, false );
      size_t sizeA = 0;
      vector< node_id > idsA;
      for (node_id u = 0; u < g.n; ++u) {
	if ( u % (g.n / 10) == 0 ) {
	  cerr << "\r                                       \r" << static_cast<double>(u) / g.n;
	}
	
	 if (unidist(gen) < q) {
	    double gainu = marge( nEvals, g, u, A );
	    
	    if (sizeA == k) {
	       node_id x;
	       double gainx = 0;
	       exchange_candidate( x, gainx, idsA );

	       if (gainu > (1.0 + c)*gainx) {
		  A[x] = false;
		  A[u] = true;

		  vector< node_id >::iterator it = find( idsA.begin(), idsA.end(), x );
		  idsA.erase( it );
		  idsA.push_back( u );
	       }
	    } else {
	       A[u] = true;
	       idsA.push_back(u);
	       ++sizeA;
	    }
	 }
      }

      double valSol = compute_valSet( --nEvals, g, A );
      g.logg << "solution value: " << valSol << endL;
      g.logg << "number of queries: " << nEvals << endL;

      reportResults( nEvals, valSol, k );
   }
};

class Al {
   public:
   Args& args;
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   size_t maxMem = 0;
   
   Al( Args& inargs ) : args(inargs), g( args.g ) {
      k = inargs.k;
      epsi = inargs.epsi;
   }

   double greedy( vector< bool >& uni, size_t k ) {
      vector<bool> A( g.n, false );
      double maxGain;
      node_id maxIdx;
      MyPair tmp;

      for (size_t i = 0; i < k; ++i) {
	 maxGain = 0;
	 for (node_id u = 0; u < g.n; ++u) {
	    if (uni[u]) {
	       if (marge( nEvals, g,u,A) > maxGain) {
		  maxIdx = u;
		  maxGain = marge( nEvals, g,u,A);
	       }
	    }
	 }

	 if (maxGain > 0) {
	    A[maxIdx] = true;
	 } else {
	    break;
	 }
      }

      return compute_valSet( nEvals, g, A );
   }
   
   void run( bool report = true ) {
      double alpha = 1.0 / 6; //Gupta et al. post
      double kappa = alpha / (1.0 + alpha);

      vector< bool > emptySet( g.n, false );
      double tau_min = 0;
      double Delta = 0;
      double LB = 0;
      deque< double > threshVals;
      deque< vector< vector< bool > > > sols;
      deque< vector< size_t > > sizes;
      deque< vector< double > > vals;

      size_t ell = (1.0/epsi + 0.5);
      
      for (node_id u = 0; u < g.n; ++u) {
	 double singletonVal = marge(nEvals, g, u, emptySet, emptySet );
	 if (singletonVal > Delta) {
	    Delta = singletonVal;
	 }
	 
	 tau_min = Delta;
	 
	 if (threshVals.size() > 0) {
	    while ( threshVals[0] < tau_min ) {
	       threshVals.pop_front();
	       sols.pop_front();
	       sizes.pop_front();
	       vals.pop_front();
	       if (threshVals.empty())
		  break;
	    }
	 } else {
	    threshVals.push_back( tau_min );
	    vector< vector< bool > > pool( ell, emptySet );
	    vector< size_t > tmpSizes( ell, 0 );
	    vector< double > tmpVals( ell, 0.0 );
	       
	    sols.push_back( pool );
	    sizes.push_back( tmpSizes );
	    vals.push_back( tmpVals );
	 }

	 double curr_thresh = tau_min;
	 size_t pos = 0;
	 while (curr_thresh <= k*Delta) {
	    if (pos >= threshVals.size()) {
	       threshVals.push_back( curr_thresh );
	       vector< vector< bool > > pool( ell, emptySet );
	       vector< size_t > tmpSizes( ell, 0 );
	       vector< double > tmpVals( ell, 0.0 );
	       
	       sols.push_back( pool );
	       sizes.push_back( tmpSizes );
	       vals.push_back( tmpVals );
	    }
	    for (size_t i = 0; i < ell; ++i) {
	       vector< bool >& curr_sol = sols[ pos ][i];

	       size_t sizeSol = sizes[ pos ][i];
	       if (sizeSol < k) {
		  double gain = marge( nEvals, g, u, curr_sol );
		  if ( gain >= curr_thresh * kappa / k) {
		     curr_sol[u] = true;
		     ++(sizes[pos][i]);
		     vals[pos][i] += gain;
		     if (vals[pos][i] > LB) {
			LB = vals[pos][i];
		     }

		     i = ell;
		  }
	       }
	    }
	    ++pos;
	    curr_thresh = curr_thresh * (1 + epsi);
	 }

	 size_t totalSize = 0;
	 for (size_t jj = 0; jj < sizes.size(); ++jj) {
	    for (size_t ii = 0; ii < sizes[jj].size(); ++ii) {
	       totalSize += sizes[jj][ii];
	    }
	 }
	 if (totalSize > maxMem)
	    maxMem = totalSize;
      }

      ///offline post-process
      double valSol = 0.0;
      for (size_t i = 0; i < sols.size(); ++i) {
	 //construct universe i
	 vector< bool > uni(g.n, false);
	 for (size_t u = 0; u < g.n; ++u) {
	    for (size_t j = 0; j < ell; ++j) {
	       if (sols[i][j][u] == true) {
		  uni[u] = true;
	       }
	    }
	 }

	 //run offline alg. on uni
	 //FTg ftg( args );
	 Br ftg( args );
	 ftg.run_mpl( uni, Delta, 1.0 / k );

	 if (ftg.solVal > valSol)
	    valSol = ftg.solVal;

	 nEvals += ftg.nEvals;
	 
      }

      g.logg << "solution value: " << valSol << endL;
      g.logg << "number of queries: " << nEvals << endL;

      reportResults( nEvals, valSol, maxMem );
   }
};






struct mypair {
   double gain;
   size_t id;
   size_t idxA;
};

struct mypairLT {
   bool operator() ( const mypair& p1, const mypair& p2 ) {
      return p1.gain > p2.gain;
   }
};

//Chakrabarti & Kale
class Ck {
public:
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   size_t c = 1;
   double solVal = 0.0;
   size_t mem = 0;
   
   Ck( Args& args ) : g( args.g ) {
      k = args.k;
      c = args.c;
   }

   void run( bool report = true ) {
      size_t sizeA = 0;
      vector<bool> cvgA( g.n, false );
      vector< node_id > idsA;
      priority_queue< mypair, vector< mypair >, mypairLT > w;
      for (node_id u = 0; u < g.n; ++u) {
	 double valA = compute_valSet( nEvals, g, idsA );
	 idsA.push_back(u);
	 double valAadd = compute_valSet( nEvals, g, idsA );
	 idsA.pop_back();
	 double gain = valAadd - valA;
	 if (sizeA < k) {
	    mypair tmp;
	    tmp.id = u;
	    tmp.gain = gain;
	    tmp.idxA = sizeA;
	    w.push( tmp );
	    ++sizeA;
	    idsA.push_back( u );
	 } else {
	    mypair tmp = w.top();
	    if (gain >= 2*tmp.gain) {
	       idsA[ tmp.idxA ] = u;
	       w.pop();
	       tmp.id = u;
	       tmp.gain = gain;
	       w.push( tmp );
	    }
	 }
      }
	 
      double valSol = compute_valSet( --nEvals, g, idsA );

      g.logg << "nEvals: " << nEvals << endL;
      g.logg << "valSol: " << valSol << endL;

      if (report)
	 reportResults( nEvals, valSol, sizeA );

      solVal = valSol;
   }

};




//ThresholdGreedy
class Tg {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
public:
   Tg( Args& args ) : g( args.g ) {
      k = args.k;
      epsi = args.epsi;
   }

   void run() {

      vector< bool > A( g.n, false );
      vector< bool > cvgA( g.n, false );
      size_t valA = 0;
      size_t sizeA = 0;

      //Get max singleton
      g.logg << "ThresholdGreedy: Determining max singleton..." << endL;
      double M = 0;
      for (size_t x = 0; x < g.n; ++x) {
	 if ( marge( nEvals, g, x, A, cvgA ) > M ) {
	    M = marge( nEvals, g, x, A, cvgA );
	 }
      }

      g.logg << "M= " << M << endL;
      
      double tau = M;
      double maxGain;      
      while (tau >= epsi*M / k) {
	 maxGain = 0;
	 tau = tau * ( 1 - epsi );
	 for (node_id u = 0; u < g.n; ++u) {
	    double gain = marge(nEvals, g, u, A, cvgA);
	    if (gain >= tau) {
	       A[u] = true;
	       g.coverAdjacent( u, cvgA );
	       valA += gain;
	       ++sizeA;
	    }
	    if (gain > maxGain) {
	       maxGain = gain;
	    }
	    if (sizeA == k)
	       break;
	 }
	 if (sizeA == k)
	    break;

	 if (maxGain < tau)
	    tau = maxGain;
      }
      
      g.logg << "Evals: " << nEvals << endL;
      size_t tmp = 0;
      size_t solVal = compute_valSet( tmp, g, A );
      g.logg << "Solution Value: " << solVal << endL;

      if (solVal != valA) {
	 cerr << "ERROR\n";
      }
	 
      reportResults( nEvals, solVal );
   }
};

//Sieve-Streaming++
class Ss {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   size_t maxMem = 0;
   
   public:
   Ss( Args& args ) : g( args.g ) {
      k = args.k;
      epsi = args.epsi;
   }

   size_t sizeSet( vector<bool>& S ) {
      size_t ssize = 0;
      for (size_t i = 0; i < g.n; ++i) {
	 if (S[i])
	    ++ssize;
      }
      return ssize;
   }
   
   void run() {
      vector< bool > emptySet( g.n, false );
      double tau_min = 0;
      double Delta = 0;
      double LB = 0;

      deque< double > threshVals;
      deque< vector< bool > > sols;
      deque< vector< bool > > cvgs;
      deque< size_t > sizes;
      deque< double > vals;
      
      for (node_id u = 0; u < g.n; ++u) {
	 double singletonVal = marge(nEvals, g, u, emptySet, emptySet );
	 if (singletonVal > Delta) {
	    Delta = singletonVal;
	 }

	 double maxLD = LB;
	 if (Delta > maxLD)
	    maxLD = Delta;
	 
	 tau_min = maxLD / (2.0*k);

	 if (threshVals.size() > 0) {
	    while ( threshVals[0] < tau_min ) {
	       threshVals.pop_front();
	       sols.pop_front();
	       cvgs.pop_front();
	       maxMem -= sizes[0];
	       sizes.pop_front();
	       vals.pop_front();
	    }
	 } else {
	    threshVals.push_back( tau_min );
	    sols.push_back( emptySet );
	    cvgs.push_back( emptySet );
	    sizes.push_back( 0 );
	    vals.push_back( 0 );
	 }

	 double curr_thresh = tau_min;
	 size_t pos = 0;
	 while (curr_thresh <= Delta) {
	    if (pos >= threshVals.size()) {
	       threshVals.push_back( curr_thresh );
	       sols.push_back( emptySet );
	       cvgs.push_back( emptySet );
	       sizes.push_back( 0 );
	       vals.push_back( 0 );
	    }
	    vector< bool >& curr_sol = sols[ pos ];
	    vector< bool >& curr_cvg = cvgs[ pos ];
	    size_t sizeSol = sizes[ pos ];
	    if (sizeSol < k) {
	       double gain = marge( nEvals, g, u, curr_sol, curr_cvg);
	       if ( gain >= curr_thresh) {
		  curr_sol[u] = true;
		  ++(sizes[pos]);
		  g.coverAdjacent( u, curr_cvg );
		  vals[pos] += gain;
		  ++maxMem;
		  if (vals[pos] > LB) {
		     LB = vals[pos];
		  }
	       }
	    }
	    ++pos;
	    curr_thresh = curr_thresh * (1 + epsi);
	 }
      }

      double valSol = 0;
      size_t tmpst = 0;
      for (size_t i = 0; i < vals.size(); ++i) {
	 if (sizeSet( sols[i] ) > k) {
	    cerr << "ERROR, sol too large\n";
	    return;
	 }
	 if (vals[i] != compute_valSet( nEvals, g, sols[i] )) {
	    //cerr << "ERROR, values incorrect:"
	    //<< vals[i] << ' ' << compute_valSet( tmpst, g, sols[i] ) << endl;
	    vals[i] = compute_valSet( tmpst, g, sols[i] );
	 }
	 if (vals[i] > valSol) {
	    valSol = vals[i];
	 }
      }

      g.logg << "solution value: " << valSol << endL;
      g.logg << "number of queries: " << nEvals << endL;
      g.logg << "memory used: " << maxMem << endL;
      reportResults( nEvals, valSol, maxMem );
   }
};

//Buchbinder, Feldman, Schwartz 2018
class Bs {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
public:
   Bs( Args& args ) : g( args.g ) {
      k = args.k;
   }
   
   void run() {
      vector<bool> A( g.n, false );
      vector<size_t> idsA;
      size_t sizeA = 0;
      vector<bool> cvgA( g.n, false );
      deque<size_t> Aprime;
      
      size_t valA = 0;
      
      for (node_id u = 0; u < g.n; ++u) {
	 if (sizeA < k) {
	    A[u] = true;
	    valA += g.coverAdjacent( u, cvgA );
	    ++sizeA;
	 } else {
	    size_t maxIdx = 0;
	    size_t maxVal = 0;
	    size_t idx = 0;
	    for (size_t i = 0; i < k; ++i) {
	       while ( A[idx] == false )
		  ++idx;
	       A[u] = true;
	       A[idx] = false;
	       size_t tmpVal = compute_valSet( nEvals, g, A );
	       if (tmpVal > maxVal) {
		  maxIdx = idx;
		  maxVal = tmpVal;
	       }
	       A[u] = false;
	       A[idx++] = true;
	    }
	    if (maxVal - valA >= valA / k) {
	       A[u] = true;
	       A[maxIdx] = false;
	       valA = maxVal;
	    }
	 }
      }

      g.logg << "Evals: " << nEvals << endL;

      size_t tmpEvals = 0;
      
      g.logg << "f(A): " << compute_valSet( tmpEvals,  g, A ) << endL;
      g.logg << "size of A: " << sizeA << endL;

      reportResults( nEvals, compute_valSet(tmpEvals, g, A) );
   }
};


//P-Pass
class Pp {
   size_t k;
   tinyGraph& g;
   size_t nEvals = 0;
   double epsi;
   size_t P;
   double tradeoff;
   
   public:
   Pp( Args& args ) : g( args.g ) {
      k = args.k;
      tradeoff = args.tradeoff;
      epsi = tradeoff*args.epsi;
      P = 1;//args.P;
      double factor = (double)P / (P + 1);
      double bound = 1/exp(1) + args.epsi*(1 - tradeoff);
      //bound = 1.0 / bound;
      bound = log( bound );
      //cerr << bound << endl;
      //cerr << factor << endl;
      while ( P * log( factor) > bound ) {
	 //cerr << P * log( factor ) << endl;
	 ++P;
	 factor = (double)P / (P + 1);
      }

      g.logg << "P-Pass initialized, P=" << P << endL;
   }

   size_t sizeSet( vector<bool>& S ) {
      size_t ssize = 0;
      for (size_t i = 0; i < g.n; ++i) {
	 if (S[i])
	    ++ssize;
      }
      return ssize;
   }
   
   void run() {
      vector< bool > emptySet( g.n, false );
      double tau_min = 0;
      double Delta = 0;

      deque< double > threshVals; //(1,0.0);
      deque< vector< bool > > sols;// (1, vector<bool>());
      deque< vector< bool > > cvgs;// (1, vector<bool>());
      deque< size_t > sizes; // (1,0);
      deque< double > vals; // (1, 0.0);

      double factor = static_cast<double>( P ) / (P + 1);
      double T = factor;
      g.logg << "P/(P+1) = " << factor << endL;
      //Do the first pass
      for (node_id u = 0; u < g.n; ++u) {
	 double singletonVal = marge(nEvals, g, u, emptySet, emptySet );
	 if (singletonVal > Delta) {
	    Delta = singletonVal;
	 }

	 double maxLD = Delta;
	 
	 tau_min = maxLD;

	 if (threshVals.size() > 0) {
	    while ( threshVals[0] < tau_min ) {
	       threshVals.pop_front();
	       sols.pop_front();
	       cvgs.pop_front();
	       sizes.pop_front();
	       vals.pop_front();
	    }
	 } else {
	    threshVals.push_back( tau_min );
	    sols.push_back( emptySet );
	    cvgs.push_back( emptySet );
	    sizes.push_back( 0 );
	    vals.push_back( 0 );
	 }

	 double curr_thresh = tau_min;
	 size_t pos = 0;
	 while (curr_thresh <= Delta * k / T) {
	    if (pos >= threshVals.size()) {
	       threshVals.push_back( curr_thresh );
	       sols.push_back( emptySet );
	       cvgs.push_back( emptySet );
	       sizes.push_back( 0 );
	       vals.push_back( 0 );
	    }
	    vector< bool >& curr_sol = sols[ pos ];
	    vector< bool >& curr_cvg = cvgs[ pos ];
	    size_t sizeSol = sizes[ pos ];
	    if (sizeSol < k) {
	       double gain = marge( nEvals, g, u, curr_sol, curr_cvg);
	       if ( gain >= curr_thresh * T / k) {
		  curr_sol[u] = true;
		  ++(sizes[pos]);
		  g.coverAdjacent( u, curr_cvg );
		  vals[pos] += gain;
	       }
	    }
	    ++pos;
	    curr_thresh = curr_thresh * (1 + epsi);
	 }
      }

      //Do remaining P - 1 passes
      for (size_t i = 0; i < P - 1; ++i) {
	 T = T * factor;
	 for (node_id u = 0; u < g.n; ++u) {
	    double curr_thresh = threshVals[0];
	    size_t pos = 0;
	       while (curr_thresh <= Delta * k / T) {
		  if (pos >= threshVals.size()) {
		     threshVals.push_back( curr_thresh );
		     sols.push_back( emptySet );
		     cvgs.push_back( emptySet );
		     sizes.push_back( 0 );
		     vals.push_back( 0 );
		  }

		  vector< bool >& curr_sol = sols[ pos ];
		  vector< bool >& curr_cvg = cvgs[ pos ];
		  size_t sizeSol = sizes[ pos ];
		  if (sizeSol < k) {
		     double gain = marge( nEvals, g, u, curr_sol, curr_cvg);
		     if ( gain >= curr_thresh * T / k) {
			curr_sol[u] = true;
			++(sizes[pos]);
			g.coverAdjacent( u, curr_cvg );
			vals[pos] += gain;
		     }
		  }
		  curr_thresh = curr_thresh * (1 + epsi);
		  ++pos;
	       }
	 }
      }

      
      double valSol = 0;
      for (size_t i = 0; i < vals.size(); ++i) {
	 size_t tmpst = 0;
	 vals[i] = compute_valSet( tmpst, g, sols[i] );
	 if (vals[i] > valSol) {
	    valSol = vals[i];
	 }
      }

      g.logg << "solution value: " << valSol << endL;
      g.logg << "number of queries: " << nEvals << endL;

      reportResults( nEvals, valSol );
   }
};

class Fig {
   size_t k;
   tinyGraph& g;
   bool steal;
   size_t nEvals = 0;
   double epsi;
   double stopGain;
   bool lazy;
public:
   Fig( Args& args ) : g( args.g ) {
      k = args.k;
      steal = args.plusplus;
      epsi = args.epsi;
      lazy = args.lazy;
   }

   long leastBenefit( node_id u, vector<bool>& set ) {
      set[u] = false;
      long m = marge( nEvals, g, u, set );
      set[u] = true;
      return m;
   }

   bool swap( node_id u, node_id v, vector<bool>& set ) {
      long init = compute_valSet( nEvals, g, set );
      set[u] = false;
      set[v] = true;
      long m = compute_valSet( nEvals, g, set );
      if (m > init) {
	 return true;
      }
      set[u] = true;
      set[v] = false;
      return false;
   }

   bool fast_swap( node_id u, node_id v, vector<bool>& set ) {
      set[u] = false;
      set[v] = true;
      return true;
   }

   size_t sizeSet( vector<bool>& S ) {
      size_t ssize = 0;
      for (size_t i = 0; i < g.n; ++i) {
	 if (S[i])
	    ++ssize;
      }
      return ssize;
   }
   
   void add( vector<bool>& S, vector<bool>& T, node_id& j, size_t& tau, vector< double >& pastGains ) {
      if (sizeSet( S ) == k) {
	 j = 0;
	 tau = stopGain;
	 return;
      }

      while ( tau > stopGain ) {
	 for (node_id x = j; x < g.n; ++x) {
	    if (!T[x]) {
	       if (pastGains[x] >= tau || !lazy) {
		  double gain =marge(nEvals, g, x, S);
		  pastGains[x] =gain;
		  if (gain >= tau ) {
		     S[ x ] = true;
		     j = x;
		     return;
		  }

	       }
	    }
	 }
	 tau = ( 1 - epsi ) * tau;
	 j = 0;
      }
      j = 0;
      return;
   }
  
   void run() {
      vector<bool> A( g.n, false );
      vector<bool> B( g.n, false );
      vector<bool> C( g.n, false );
      vector<bool> D( g.n, false );
      vector<bool> E( g.n, false );

      g.logg << "FIG: epsi = " << epsi << ", k = " << k << endL;
      
      //Get max singleton
      g.logg << "FIG: Determining max singleton..." << endL;
      size_t M = 0;
      node_id a0;
      vector< double > pastGainsA(g.n, 0);
      vector< double > pastGainsB(g.n, 0);
      for (size_t x = 0; x < g.n; ++x) {
	 double gain =marge( nEvals, g, x, A );
	 if ( gain > static_cast<signed long>(M) ) {
	    pastGainsA[ x ] = gain;
	    pastGainsB[ x ] = gain;
	    a0 = x;
	    M = marge( nEvals, g, x, A );
	 }
      }

      g.logg << "FIG: M = " << M << endL;
      g.logg << "FIG: Stopping condition: " << stopGain << endL;
      
      g.logg << "FIG: Starting first interlacing..." << endL;
      size_t tauA = M;
      size_t tauB = M;
      stopGain = epsi * M / g.n;
      node_id a = 0;
      node_id b = 0;
      while ( tauA > stopGain || tauB > stopGain) {
	 //g.logg << "FIG: tauA = " << tauA << ", tauB = " << tauB << endL;
	 add( A, B, a, tauA, pastGainsA );
	 add( B, A, b, tauB, pastGainsB );
      }

      //g.logg << "FIG: First interlacing complete." << endL;
      g.logg << "FIG: Starting second interlacing..." << endL;
      size_t tauD = M;
      size_t tauE = M;
      node_id d = 0;
      node_id e = 0;
      D[ a0 ] = true;
      E[ a0 ] = true;
      pastGainsA.assign(g.n, DBL_MAX);
      pastGainsB.assign(g.n, DBL_MAX);
      while ( tauD > stopGain || tauE > stopGain) {
	 //g.logg << "FIG: tauD = " << tauD << ", tauE = " << tauE << endL;
	 add( D, E, d, tauD, pastGainsA );
	 add( E, D, e, tauE, pastGainsB );
      }

      g.logg << "FIG: Second interlacing complete." << endL;
      
      size_t valA = compute_valSet( nEvals,  g, A );
      size_t valB = compute_valSet( nEvals,  g, B );
      size_t valD = compute_valSet( nEvals,  g, D );
      size_t valE = compute_valSet( nEvals,  g, E );
      vector <size_t> vC;
      vC.push_back( valA );
      vC.push_back( valB );
      vC.push_back( valD );
      vC.push_back( valE );

      size_t valC = 0;
      size_t jj = 0;
      for (size_t i = 0; i < vC.size(); ++i) {
	 if (vC[i] > valC) {
	    valC = vC[i];
	    jj = i;
	 }
      }
      
      if (jj == 0) 
	 C = A;
      if (jj == 1)
	 C = B;
      if (jj == 2)
	 C = D;
      if (jj == 3)
	 C = E;
      g.logg << "FIG: f(C) = " << compute_valSet( nEvals,  g, C ) << endL;

      //steal      
      if (steal) {
	 vector< MyPair > possibleGain;
	 vector< MyPair > Cbenefits;
	 MyPair tmp;
	 for (size_t i = 0; i < g.n; ++i) {
	    if (A[i] || B[i] || D[i] || E[i]) {
	       tmp.u = i;
	       tmp.gain = marge( nEvals, g, i, C );

	       
	       possibleGain.push_back( tmp );
	    }

	    if (C[i]) {
	       tmp.u = i;
	       tmp.gain = leastBenefit(i,C);

	       Cbenefits.push_back ( tmp );
	    }
	 }

	 g.logg << "FIG: Sorting..." << endL;
	 std::sort( Cbenefits.begin(), Cbenefits.end(), gainLT() );
	 std::sort( possibleGain.begin(), possibleGain.end(), revgainLT() );


	 g.logg << "FIG: Swapping..." << endL;
	 //Attempt to replace elements of C
	 size_t nStolen = 0;
	 for (size_t i = 0; i < Cbenefits.size(); ++i) {

	    if ( Cbenefits[ i ].gain < possibleGain[ i ].gain ) {
	       
	       if ( C[ possibleGain[i].u ] ) {
		  C[ possibleGain[i].u ] = false;
	       } else {
		  if (this->fast_swap( Cbenefits[i].u,
				       possibleGain[i].u,
				       C )) {
		     ++nStolen;
		     //C[ Cbenefits[i].u ] = false;
		     //C[ possibleGain[i].u ] = true;
		  }
	       }
	    }
	 }
	 g.logg << "FIG: Stealing complete: " << nStolen << " stolen." << endL;
	 g.logg << "FIG: f(C) = " << compute_valSet( nEvals,  g, C ) << endL;
      }

      g.logg << "FIG: # evals = " << nEvals << endL;

      g.logg << "Size of set: " << this->sizeSet( C ) << endL;
      reportResults( nEvals, compute_valSet(nEvals, g, C) );
   }
};

vector< bool > emptyBoolVector;
vector< size_t> emptySize_tVector;
vector< double > emptyDoubleVector;

class Latg {
public:
   random_device rd;
   mt19937 gen;
   size_t k;
   tinyGraph& g;
   double epsi;
   double delta;
   size_t nEvals = 0;
   bool fast = true;
   bool lazy;
  size_t c = 0;
   bool reportRounds = false;
   Latg( Args& args ) : gen( rd() ), g( args.g ) {
     k = args.k;
      epsi = args.epsi;
      delta = args.delta;
      lazy = args.lazy;
      g.logg << "LATG initialized:" << endL;
      g.logg << "epsi=" << epsi << endL;
      g.logg << "delta=" << delta << endL;
      g.logg << "k=" << k << endL;
      g.logg << "lazy=" << lazy << endL;
      if (fast) {
	g.logg << WARN << "Fast mode enabled. Theoretical guarantees will not hold!" << endL;

	c = 1 / epsi;
      } else {
	c = 8 / epsi;
	epsi = 0.63 * epsi / 8;
	delta = delta / 2;
      }
   }

void filter( size_t& nEvals,
	     tinyGraph& g,
	     vector< bool >& A,
	     vector< bool >& S,
	     double tau,
	     vector< size_t >& idsA,
	     vector< double >& pastGains,
	     bool lazy ) {
  vector< size_t > newIdsA;
  for (size_t i = 0; i < idsA.size(); ++i) {
    size_t x = idsA[i];
    if (S[x]) {
      // filter x
      A[x] = false;
    } else {
       double gain;
       if (pastGains[x] >= tau || !lazy) {
	  gain = marge( nEvals, g, x, S );
	  pastGains[x] = gain;
       }
       else {
	  gain = pastGains[x];
       }
      if ( gain < tau) {
	//filter x
	A[x] = false;
      } else {
	//keep x
	newIdsA.push_back( x );
      }
    }
  }

  idsA.swap( newIdsA );
}

unsigned samp_Dt( size_t& nEvals,
		  tinyGraph& g,
		  vector< size_t >& idsA,
		  vector< size_t >& idsS,
		  size_t t,
		  double tau ) {
  vector< size_t > idsT;
  vector< bool > T( g.n, false );
  uniform_int_distribution<size_t> dist(0, idsA.size() - 1);
  while (idsT.size() < t - 1) {
    size_t pos;
    pos = dist( gen );
    if ( !T[ idsA[ pos ] ] ) {
      T[ idsA[ pos ] ] = true;
      idsT.push_back( idsA[ pos ] );
    }
  }

  size_t x = 0;
  do {
    size_t pos = dist( gen );
    x = idsA[ pos ];
  } while ( T[ x ] );

  vector< bool > ScupT(g.n, false);
      
  for (size_t i = 0; i < idsS.size(); ++i) {
    ScupT[idsS[i]] = true;
  }
  for (size_t i = 0; i < idsT.size(); ++i) {
    ScupT[idsT[i]] = true;
  }

  if ( marge( nEvals, g, x, ScupT ) >= tau ) {

    return 1;
  }

  return 0;
}

void sampleUt( vector<size_t>& R, vector<size_t>& A, size_t t ) {
  //g.logg << "Starting sampleUX..." << endL;
  R.clear();
  uniform_int_distribution<size_t> dist(0, A.size() - 1);
  vector< bool > alreadySampled( A.size(), false );

  for (size_t i = 0; i < t; ++i) {
    size_t pos;
    do {
      pos = dist( gen );
    } while ((alreadySampled[pos]) );

    alreadySampled[pos] = true;
    R.push_back( A[pos] );
  }
}

double reduced_mean_dagum( size_t& nEvals,
			   tinyGraph& g,
			   vector< size_t >& idsA,
			   vector< size_t >& idsS,
			   size_t t,
			   double tau,
			   double epsi,
			   double delta,
			   bool fast = false) {
  g.logg << DEBUG << "RMD: Chernoff requires: " << 16*( log( 2 / delta )/ epsi / epsi + 1 ) << endL;
  g.logg << DEBUG << "t: " << t << endL;
											       
  
  double lambda = exp(1) - 2.0;
  double upsilon = 4.0 * lambda * log(2.0 / delta) / pow(epsi, 2);
  double upsilon_1;
  if (!fast)
    upsilon_1 = 1.0 + (1.0 + epsi) * upsilon;
  else
    upsilon_1 = 100;
  
  size_t sum = 0;
  size_t nSamps = 0;
  size_t goal = ceil(upsilon_1);
											  
  while(sum < goal) {
    nSamps += 1;
    sum += samp_Dt( nEvals, g, idsA, idsS, t, tau );

    if (static_cast<double>( goal ) / nSamps < (1.0 - 1.5*epsi)) {
      break;
    }
  }
  
    g.logg << DEBUG << "Dagum required: " << nSamps << endL;
						       g.logg << DEBUG << "Dagum returning: " << static_cast<double>( goal ) / nSamps << endL;
  return static_cast<double>( goal ) / nSamps;
}

void report_rounds( vector< bool >& S,
		    vector< double >& valRounds,
		    tinyGraph& g ) {
   // size_t tmp = 0;
   // if (valRounds.size() == 0) {
   //    valRounds.push_back( compute_valSet( tmp, g, S ) );
   //    return;
   // }
   
   // size_t tmpVal = valRounds[ valRounds.size() - 1 ];
   // size_t tmpVal2 = compute_valSet( tmp, g, S );
   // if (tmpVal2 > tmpVal) {
   //    valRounds.push_back( tmpVal2 );
   // } else {
   //    valRounds.push_back( tmpVal );
   // }
   valRounds.push_back( 0 );
}


   
   void threshold_sample( size_t& nEvals,
		       tinyGraph& g,
		       vector< bool >& S,
		       size_t k,
		       double tau,
		       double epsi,
		       double delta,
		       vector< bool >& exclude,
		       bool bexcl,
		       vector< size_t >& idsS,
		       bool fast,
		       bool returnA = false,
		       vector< bool >& retA = emptyBoolVector,
		       vector< size_t >& retidsA = emptySize_tVector,
		       bool reportRounds = false,
		       vector< double >& valRounds = emptyDoubleVector,
		       vector< double >& pastGains = emptyDoubleVector,
		       bool lazy = false ) {
  double hatepsi;
  if (!fast) {
    hatepsi = epsi / 3;
  } else {
    hatepsi = epsi;
  }
  g.logg << DEBUG << "hatepsi = " << hatepsi << endL;
  size_t r = log( 2*g.n / delta ) + 1;
  size_t m = log( k ) / hatepsi + 1;
  double hatdelta = delta / (2*r*(m+1));
  size_t adaRounds = 0;

  size_t sizeS = 0;
  vector< bool > A( g.n, true );
  vector< size_t > idsA;
  //vector< size_t > idsS;

  vector< size_t > idsT;
  for (size_t i = 0; i < g.n; ++i) {
    if (bexcl) {
      if (!exclude[i])
	idsA.push_back(i);
      else
	A[i] = false;
    } else
      idsA.push_back( i );
  }

  filter( nEvals, g, A, S, tau, idsA, pastGains, lazy );
  //nothing added during filter
  if (reportRounds) {
     report_rounds( S, valRounds, g );
     ++adaRounds;
  }

  // if (idsA.size() <= log( g.n )) {
  //   can run in sequential mode while maintaining log(n)-
  //   adaptivity
  //   for (size_t j = 0; j < idsA.size(); ++j) {
  //     size_t& x = idsA[j];
  //     if (!S[x]) {
  // 	 signed long margeTmp = marge(nEvals, g, x, S);
  // 	if (margeTmp >= static_cast<signed long>( tau )) {
  // 	  S[x] = true;
  // 	  ++sizeS;
  // 	  idsS.push_back( x );
	  
  // 	} else {

  // 	}

  // 	if (reportRounds) {
  // 	   report_rounds( S, valRounds, g );
  // 	}
	
  // 	if (sizeS >= k) {
  // 	  if (returnA) {
  // 	    retA.swap( A );
  // 	    retidsA.swap( idsA );
  // 	  }
  // 	  return;
  // 	}
  //     }
  //   }

  //   if (returnA) {
  //     retA.swap( A );
  //     retidsA.swap( idsA );
  //   }
    
  //   return;
  // }
  
  for (unsigned j = 0; j < r; ++j) {

    g.logg << DEBUG << "Post filter size of A: " << idsA.size() << endL;
    //	 g.logg << DEBUG << "(1 - hatepsi): " << 1 - hatepsi << endL;
    //  g.logg << DEBUG << "log n = " << log( g.n ) << endL;
						   
    if (idsA.size() == 0) {
      return;
    } else {
      if (idsA.size() == 1) {

	S[ idsA[0] ] = true;
	idsS.push_back( idsA[0] );
	++sizeS;
	
	return;
      }
    }
    double tmpT = 1;//= (1+hatepsi)^i
    size_t t = 0;
    for (unsigned i = 0; i < m; ++i) {
	    
      t = idsA.size();
      if (t > static_cast<size_t>(tmpT))
	t = static_cast<size_t>(tmpT);
      double oldTmpT = tmpT;
      if (fast) {
	 tmpT = tmpT * (1 + hatepsi);//	 tmpT = tmpT * 2;
      } else {
	 tmpT = tmpT * (1 + hatepsi);
      }
      if (tmpT < oldTmpT + 1)
	tmpT = oldTmpT + 1;
      
      double ubar = 0;

      if (t > k - sizeS)
	  break;
      
      if ( t > 1) {
	
	ubar = reduced_mean_dagum( nEvals,
				   g,
				   idsA,
				   idsS,
				   t,
				   tau,
				   hatepsi,
				   hatdelta,
				   fast );

	
	if (ubar <= 1 - 1.5*hatepsi)
	  break;

	if (t == idsA.size()) {
	  //all possible elements are good in expectation
	  //can break
	  break;
	}

	
      } 
    }

    if ( t > k - sizeS )
      t = k - sizeS;

	 
    sampleUt( idsT, idsA, t );
    g.logg << DEBUG << "Adding random set of size: "
	   << t << " " << idsT.size() << endL;
    for (size_t i = 0; i < t; ++i){
      if (!S[ idsT[i] ]) {
	S[ idsT[i] ] = true;
	++sizeS;
	idsS.push_back( idsT[i] );
      }
	    
    }

    if (reportRounds) {
       report_rounds( S, valRounds, g );
       ++adaRounds;
    }

    if (sizeS >= k) {
      if (returnA) {
	retA.swap( A );
	retidsA.swap( idsA );
      }
      return;
    }

    if (j < r - 1) {
       filter( nEvals, g, A, S, tau, idsA, pastGains, lazy );
       if (reportRounds) {
	  report_rounds( S, valRounds, g );
	  ++adaRounds;
       }
    }
  }
  
  if (returnA) {
    retA.swap( A );
    retidsA.swap( idsA );
  }
}

   size_t get_size_set( vector< bool >& S ) {
      size_t sizeSol = 0;
      for (size_t i = 0; i < g.n; ++i)
	 if (S[i])
	    ++sizeSol;
      return sizeSol;
   }
   
   void run() {
     g.logg << INFO << "LATG is starting run..." << endL;
      vector<bool> A( g.n, false );
      vector<size_t> idsA;
      vector<size_t> idsB;
      vector<bool> B( g.n, false );
      vector<bool> C( g.n, false );
      vector<bool> sol( g.n, false );
      double solVal = 0;

      //Get max singleton
      g.logg << "Determining max singleton M..." << endL;
      double M = 0;
      //node_id a0;

      vector< double > pastGains(g.n, 0);
      for (size_t x = 0; x < g.n; ++x) {
	 double gain = marge( nEvals, g, x, A );
      	 if ( gain > (M) ) {
      	    M = marge( nEvals, g, x, A );
      	 }
	 pastGains[x] = gain;
      }

      g.logg << "M = " << M << endL;
      double d = 1.0;
      
      size_t m = log( 1.0 / (c * k) ) / log( pow(1 - epsi, d) );

      double tau_i = M / (1 - epsi);

      g.logg << DEBUG << "m = " << m << endL << INFO;

      if (!fast) {
	 delta = delta / m;
      }
      
      vector< double > valueThisRound;

      valueThisRound.push_back( 0 ); //Finding topKgains was first adaptive round
      for (unsigned i = 0; i <= m; ++i) {
	 tau_i = tau_i * pow(1 - epsi, d);
	 
	threshold_sample( nEvals, g, A, k - idsA.size(),
			  tau_i,
			  epsi, delta,
			  A,
			  false,
			  idsA, fast,
			  false,
			  emptyBoolVector,
			  emptySize_tVector,
			  reportRounds,
			  valueThisRound,
			  pastGains,
			  lazy);

	if (idsA.size() == k)
	  break;
      }

      tau_i = M / (1 - epsi);
      pastGains.assign( g.n, DBL_MAX );
      for (unsigned i = 0; i <= m; ++i) {

	 tau_i = tau_i * pow(1 - epsi, d);
	 
	threshold_sample( nEvals, g, B, k - idsB.size(),
			  tau_i,
			  epsi, delta,
			  A,
			  true,
			  idsB, fast,
			  false,
			  emptyBoolVector,
			  emptySize_tVector,
			  reportRounds,
			  valueThisRound,
			  pastGains,
			  lazy);

	if (idsB.size() == k)
	  break;
      }

      
      random_set( g, C, idsA );

      double tempVal = compute_valSet( nEvals, g, A );

      g.logg << DEBUG << "f(A)=" << tempVal << endL;
      if ( tempVal >= solVal ) {
	solVal = tempVal;
	sol = A;

      }

      tempVal = compute_valSet( nEvals, g, B );
      g.logg << DEBUG << "f(B)=" << tempVal << endL;
      if ( tempVal >= solVal ) {
	solVal = tempVal;
	sol = B;

      }

      tempVal = compute_valSet( nEvals, g, C );
      g.logg << DEBUG << "f(C)=" << tempVal << endL;
      if ( tempVal >= solVal ) {
	solVal = tempVal;
	sol = C;

      }

      g.logg << INFO << "LATG: solVal=" << solVal << endL;
      g.logg << INFO << "LATG: queries=" << nEvals << endL;
      g.logg << INFO << "LATG: solSize=" << get_size_set( sol ) << endL;

      size_t rounds = 0;
      

      if (reportRounds) {

	 g.logg << INFO << "LATG: Adaptive rounds=" << valueThisRound.size() << endL;
	 rounds = valueThisRound.size();
	 //for (size_t j = 0; j < valueThisRound.size(); ++j) {
	 //allResults.add( to_string( j ), valueThisRound[ j ] );
	 //}
      } else {
	 
      }

      reportResults( nEvals, solVal, rounds );
   }
};

#endif
