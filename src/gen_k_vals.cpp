#include "mygraph.cpp"
#include <iostream>
#include <string>

using namespace std;
using namespace mygraph;

int main( int argc, char** argv ) {
   if (argc < 2) {
      cout << "Usage: " << argv[0] << " <input file> " << endl;
      cout << "Input is undirected graph in binary format\n";
      exit(1);
   }
   
  string inputFile( argv[1] );
  tinyGraph h;
  h.read_bin( inputFile );

  double k = 50;
  do {
    cout << k << ' ';
    k=k*1.5;
    //1.0/(1.0 + sqrt(2) + 1); //cardinality
  } while (k < h.n / (2 + sqrt(2)));

  return 0;
}
